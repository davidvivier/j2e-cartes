package com.cpe.springboot.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.model.CardModel;

public interface CardRepository extends CrudRepository<CardModel, Integer> {

	/**
	 * Recherche les cartes d'un utilisateur
	 * @param login
	 * @return
	 */
	public List<CardModel> findByOwnerLogin(String ownerLogin);
	
	public List<CardModel> findByBought(boolean bought);
}
