package com.cpe.springboot.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.model.CardModel;

@RestController
public class CardRestController {
	
	private RestTemplate rest = new RestTemplate();
	
	private final String BASE_URL = "http://127.0.0.1:";
	
	private final String FACADE_PORT = "8081";
	private final String USER_PORT = "8082";
	private final String CARD_PORT = "8083";
	private final String ROOM_PORT = "8084";
	
	/* MODELE POUR REQUETES GET et POST
	 //appel depuis le controller
	 //on doit mettre les paramètres dans une Map
	 
		HashMap<String, String> params = new HashMap<>();
		// remplacez avec vos paramètres
	 	params.put("login", login);
		params.put("pwd", pwd);
		params.put("name", name);
		// choisir GET ou POST
		// remplacer l'url (sans le host)
		// remplacer le port
		// remplacer le type de retour
		String res = get("/test", FACADE_PORT, params, String.class);
		String res = post("/test", FACADE_PORT, params, String.class);
	*/

	
	private <T> T post(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> requestBody = new HttpEntity<>("", headers);
		T res = rest.postForObject(BASE_URL + target + url, requestBody, responseType, params);
		return res;
	}
	
	private <T> T get(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		T res = rest.getForObject(BASE_URL + target + url, responseType, params);
		return res;
	}
	

	@Autowired
	private CardService cardService;
	
	@RequestMapping(path="/cards", produces="application/json")
	private List<CardModel> getAllCards() {
		return cardService.getAllCards();
	}
	
	@RequestMapping(path="/cardsofuser/{login}", produces="application/json")
	private List<CardModel> getUserCards(@PathVariable String login) {
		return cardService.getUserCards(login);
	}
	
	@RequestMapping(path="/shop", produces="application/json")
	private List<CardModel> getShopCards() {
		return cardService.getShopCards();
	}
	
	@RequestMapping(path="/cardofuser/{card_id}/{login}", produces="application/json")
	private CardModel getUserCard(@PathVariable int card_id, @PathVariable String login) {
		return cardService.getUserCard(card_id, login);
	}
	
	@RequestMapping("/card/buy/{card_id}/user/{login}")
	private boolean buyCard(@PathVariable int card_id, @PathVariable String login) {
		HashMap<String, String> params = new HashMap<String, String>();
		CardModel card = cardService.getCard(card_id);
		
		if (card == null) return false;
		
		if(card.isBought()) return false;
		
		int cardPrice = card.getPrice();
		int userMoney = get("/user/"+login+"/money", USER_PORT, params, Integer.class);
		
		if (cardService.enoughMoney(userMoney,cardPrice)) {
			boolean isSpent = get("/user/"+login+"/spend/"+cardPrice, USER_PORT, params, Boolean.class);
			
			if (isSpent) {
				cardService.assignUser(card, login);
				
				return true;
			}
			
			return false;
		}
		 return false;
	}
	
	@RequestMapping("/card/sell/{card_id}/user/{login}")
	private boolean sellCard(@PathVariable int card_id, @PathVariable String login) {
		HashMap<String, String> params = new HashMap<String, String>();
		CardModel card = cardService.getCard(card_id);
		
		int cardPrice = card.getPrice();
		
		if (card.isBought() && card.getOwnerLogin().equals(login)) {
		
			boolean res = get("/user/"+login+"/earn/"+cardPrice, USER_PORT, params, Boolean.class);
			
			if (res) {
				cardService.unassignUser(card,login);
				
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	@RequestMapping("/card/{id}")
	private CardModel getCard(@PathVariable int id) {
		return cardService.getCard(id);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public void addCard(@RequestBody CardModel card) {
		cardService.addCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
	public void updateCard(@RequestBody CardModel card,@PathVariable String id) {
		card.setId(Integer.valueOf(id));
		cardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/card/{id}")
	public void deleteCard(@PathVariable String id) {
		cardService.deleteCard(id);
	}
	
	

}
