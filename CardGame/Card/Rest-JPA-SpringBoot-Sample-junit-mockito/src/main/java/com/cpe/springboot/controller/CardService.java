package com.cpe.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.model.CardModel;

@Service
public class CardService {

	@Autowired
	private CardRepository cardRepository;

	public List<CardModel> getAllCards() {
		List<CardModel> cards = new ArrayList<>();
		cardRepository.findAll().forEach(cards::add);
		return cards;
	}
	
	public List<CardModel> getUserCards(String login) {
		List<CardModel> cards = new ArrayList<>();
		cardRepository.findByOwnerLogin(String.valueOf(login)).forEach(cards::add);
		return cards;
	}
	
	public List<CardModel> getShopCards() {
		List<CardModel> cards = new ArrayList<>();
		cardRepository.findByBought(Boolean.valueOf(false)).forEach(cards::add);
		return cards;
	}
	
	public CardModel getUserCard(int id, String login) {
		CardModel card = this.getCard(id);
		
		if (card.getOwnerLogin() != null) {
			if (card.getOwnerLogin().equals(login) && card.isBought()) return card;
		}
		
		return null;
	}
	
	public void assignUser(CardModel card, String login) {
		card.setBought(true);
		card.setOwnerLogin(login);
		
		this.updateCard(card);
	}
	
	public void unassignUser(CardModel card, String login) {
		card.setBought(false);
		card.setOwnerLogin(null);
		
		this.updateCard(card);
	}
	
	public boolean enoughMoney(int money, int price) {
		if (money - price >= 0) {
			return true;
		}
		return false;
	}

	public CardModel getCard(int id) {
		return cardRepository.findOne(Integer.valueOf(id));
	}

	public void addCard(CardModel card) {
		cardRepository.save(card);
	}

	public void updateCard(CardModel card) {
		cardRepository.save(card);
	}

	public void deleteCard(String id) {
		cardRepository.delete(Integer.valueOf(id));
	}

}
