package com.cpe.springboot.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cpe.springboot.model.CardModel;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardRestController.class, secure = false)
public class CardRestControllerTest {
	
	@Autowired
	private CardRestController rest;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CardService cardService;

	CardModel mockCard=new CardModel("Batman", "Bruce wayne", "DC Comics", 10, 10, 10, 10, "" , 100, 10, "user", false);
	
	
	@Test
	public void retrieveCard() throws Exception {
		Mockito.when(
				cardService.getUserCards(Mockito.anyString())).thenReturn(Arrays.asList(mockCard));
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cardsofuser/user").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());
		String expected = "[{\"id\":0,\"name\":\"Batman\",\"description\":\"Bruce wayne\",\"family\":\"DC Comics\",\"hp\":10,\"energy\":10,\"defence\":10,\"attack\":10,\"imgUrl\":\"\",\"price\":100,\"energyMax\":10,\"ownerLogin\":\"user\",\"bought\":false}]";


		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
}
