package com.cpe.springboot.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.model.CardModel;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CardServiceTest {
	
	@Autowired
	private CardRepository cardRepository;
	
	
	@Test
    public void saveCard() {
		cardRepository.save(new CardModel("Batman", "Bruce wayne", "DC Comics", 10, 10, 10, 10, "" , 100, 10, "user", false));
		List<CardModel> cards = new ArrayList<>();
		cardRepository.findAll().forEach(cards::add); 
		 assertTrue(cards.size() ==1);
		 assertTrue(cards.get(0).getName().equals("Batman"));
		 assertTrue(cards.get(0).getDescription().equals("Bruce wayne"));
		 assertTrue(cards.get(0).getFamily().equals("DC Comics"));
		 assertTrue(cards.get(0).getOwnerLogin().equals("user"));
	}

	 @Test
	    public void findUserCards() {
		 cardRepository.save(new CardModel("Batman", "Bruce wayne", "DC Comics", 10, 10, 10, 10, "" , 100, 10, "user", false));
		 List<CardModel> poneyList= cardRepository.findByOwnerLogin("user");
		 assertTrue(poneyList.size() ==1);
		 assertTrue(poneyList.get(0).getName().equals("Batman"));
	    }
	 
		@Test
	    public void findAllCards() {
			for(int i=0;i<100;i++) {
				cardRepository.save(new CardModel("Batman"+i, "Bruce wayne", "DC Comics", 10+i, 10+i, 10+i, 10+i, "" , 100, 10, "user"+i, false));
			}
			
			List<CardModel> cards = new ArrayList<>();
			cardRepository.findAll().forEach(cards::add); 
			
			assertTrue(cards.size() ==100);
			
			for(int i=0;i<100;i++) {
				String name=cards.get(i).getName();
				name.split("Batmann");
				int index=Integer.valueOf(name.split("Batman")[1]);
				 assertTrue(cards.get(i).getName().equals("Batman"+index));
				 assertTrue(cards.get(i).getDescription().equals("Bruce wayne"));
				 assertTrue(cards.get(i).getOwnerLogin().equals("user"+index));
			}
			
		}
}
