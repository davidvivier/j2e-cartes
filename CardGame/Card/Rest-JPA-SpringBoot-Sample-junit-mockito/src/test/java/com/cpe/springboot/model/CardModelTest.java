package com.cpe.springboot.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.controller.CardRepository;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CardModelTest {
	
	@Autowired
	private CardRepository cardRepository;

	 @Test
    public void createCardModel() {
		 cardRepository.save(new CardModel("Batman", "Bruce wayne", "DC Comics", 10, 10, 10, 10, "" , 100, 10, "user", false));
		 List<CardModel> cardList= cardRepository.findByOwnerLogin("user");
		 assertTrue(cardList.size() ==1);
		 assertTrue(cardList.get(0).getName().equals("Batman"));
    }
}
