var facadeURL = "";

$("#loginForm").submit(function(e) {
  e.preventDefault();
  $.ajax({
            type: "POST",
            url: facadeURL + "login",
            data: {
                username: $("#username").val(),
                password: $("#password").val()
            },
            success: function(data) {
              if (data === "badpassword") {
                alert("Bad password. Please type the correct one.");
              }
              else if (data === "loginnotfound") {
                alert("Login not found. Please type a correct one or sign up.")
              }
              else {
                document.cookie="username=" + data;
                window.location.href = "cardHome.html";
              }
            }
        });
    
  });
});