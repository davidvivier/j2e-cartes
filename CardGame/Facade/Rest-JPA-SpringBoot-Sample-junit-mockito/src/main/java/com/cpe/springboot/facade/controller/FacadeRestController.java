package com.cpe.springboot.facade.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
public class FacadeRestController {
	
	
	private RestTemplate rest = new RestTemplate();
	
	private final String BASE_URL = "http://127.0.0.1:";
	
	private final String FACADE_PORT = "8081";
	private final String USER_PORT = "8082";
	private final String CARD_PORT = "8083";
	private final String ROOM_PORT = "8084";
	
	/* MODELE POUR REQUETES GET et POST
	 //appel depuis le controller
	 //on doit mettre les paramètres dans une Map
	 
		HashMap<String, String> params = new HashMap<>();
		// remplacez avec vos paramètres
	 	params.put("login", login);
		params.put("pwd", pwd);
		params.put("name", name);
		// choisir GET ou POST
		// remplacer l'url (sans le host)
		// remplacer le port
		// remplacer le type de retour
		String res = get("/test", FACADE_PORT, params, String.class);
		String res = post("/test", FACADE_PORT, params, String.class);
	*/

	
	public <T> T post(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> requestBody = new HttpEntity<>("", headers);
		T res = rest.postForObject(BASE_URL + target + url, requestBody, responseType, params);
		return res;
	}
	
	private <T> T get(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		T res = rest.getForObject(BASE_URL + target + url, responseType, params);
		return res;
	}
	
	// USER
	
	@PostMapping("/signup")
	private String signup(@RequestParam("login") String login,
			@RequestParam("pwd") String pwd,
			@RequestParam("name") String name) {
		
			HashMap<String, String> params = new HashMap<>();
		 	//params.put("login", login);
			//params.put("pwd", pwd);
			//params.put("name", name);
			String res = get("/signup/" + login + "/" + pwd + "/" + name, USER_PORT, params, String.class);
			// on renvoie direct la réponse au client, c'est à lui de l'interpréter
			System.out.println("res:"+res);
			return res;
	}
	
	@RequestMapping("/login")
	private String login(@RequestParam("login") String login,
			@RequestParam("pwd") String pwd) {
		
			HashMap<String, String> params = new HashMap<>();
		 	//params.put("login", login);
			//params.put("pwd", pwd);
			String res = get("/login/" + login + "/" + pwd, USER_PORT, params, String.class);
			// on renvoie direct la réponse au client, c'est à lui de l'interpréter
			return res;
	}
	

	@RequestMapping("/user/info/{login}")
	private Object getUserInfo(@PathVariable String login) {
		return get("/user/info/" + login, USER_PORT, new HashMap<>(), Object.class);
	}
	
	// CARD
	
	@RequestMapping("/card/{id}")
	private Object getCard(@PathVariable int id) {
		return get("/card/" + id, CARD_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/cardsofuser/{login}")
	private Object getCardsOfUser(@PathVariable String login) {
		return get("/cardsofuser/" + login, CARD_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/cardofuser/{card}/{login}")
	private Object getCardOfUser(@PathVariable Integer card, @PathVariable String login) {
		return get("/cardofuser/" + login, CARD_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/shop")
	private Object getCards() {
		return get("/shop", CARD_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/card/buy/{card}/user/{login}")
	private Boolean buyCard(@PathVariable String card,
								  @PathVariable String login) {
		return get("/card/buy/" + card + "/user/" + login, CARD_PORT, new HashMap<>(), Boolean.class);
	}
	
	@RequestMapping("/card/sell/{card}/user/{login}")
	private Boolean sellCard(@PathVariable Integer card,
								  @PathVariable String login) {
		return get("/card/sell/" + card + "/user/" + login, CARD_PORT, new HashMap<>(), Boolean.class);
	}
	
	
	// ROOM
	
	@RequestMapping("/rooms")
	private Object getRooms() {
		return get("/rooms", ROOM_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/rooms/create/user/{login}/card/{card}")
	private Object createRoom(@PathVariable String login,
								  @PathVariable Integer card) {
		return get("/rooms/create/user/" + login + "/card/" + card, ROOM_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/rooms/join/{room}/user/{login}/card/{card}")
	private Object joinRoom(@PathVariable String room,
							@PathVariable String login,
							@PathVariable Integer card) {
		return get("/rooms/join/" + room + "/user/" + login + "/card/" + card, ROOM_PORT, new HashMap<>(), Object.class);
	}
	
	@RequestMapping("/rooms/state/{room}")
	private Object getRoomState(@PathVariable String room) {
		return get("/rooms/state/" + room, ROOM_PORT, new HashMap<>(), Object.class);
	}

}
