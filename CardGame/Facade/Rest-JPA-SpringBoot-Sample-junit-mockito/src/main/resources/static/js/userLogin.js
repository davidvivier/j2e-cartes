var facadeURL = "http://localhost:8081/";

$("#loginForm").submit(function(e) {
	e.preventDefault();
	$.post("login", {login: $("#usrInput").val(), pwd: $("#pwdInput").val()}, function(data) {
		if (data === "badpassword") {
			alert("Bad password. Please type the correct one.");
		}
		else if (data === "loginnotfound") {
			alert("Login not found. Please type a correct one or sign up.");s
		}
		else {
			document.cookie="name=" + data;
			window.location.href = "cardHome.html";
		}
	});
});

$("#signupForm").submit(function(e) {
	e.preventDefault();
	$.post("signup", {name: $("#nameInput").val(), login: $("#usrInput").val(), pwd: $("#pwdInput").val()}, function(data) {
		if (data === "exists") {
			alert("User already exists. Please enter another one.");
		}
		else {				
			// signup success, redirect to login
			window.location.href = "connection.html";
		}
	});
});


