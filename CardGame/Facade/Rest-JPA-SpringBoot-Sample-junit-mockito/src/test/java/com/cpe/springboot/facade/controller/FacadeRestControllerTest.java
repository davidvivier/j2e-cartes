package com.cpe.springboot.facade.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cpe.springboot.facade.controller.FacadeRestController;

@RunWith(SpringRunner.class)
@WebMvcTest(value = FacadeRestController.class, secure = false)
public class FacadeRestControllerTest {
	
	@Autowired
	private FacadeRestController rest;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	FacadeRestController facadeRestController;

	
	@Test
	public void testSignup() throws Exception {
		
		Mockito.when(
					facadeRestController.post(Mockito.anyString(), Mockito.anyString(), Mockito.anyObject(), Mockito.any())
				).thenReturn("user");
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/signup")
				.param("login", "user")
				.param("pwd", "azerty")
				.param("name", "Dave Bowman")
				.accept(MediaType.TEXT_PLAIN);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expected = "user";
		assertEquals(expected, result.getResponse().getContentAsString());
	}
	

	@Test
	public void testLogin() throws Exception {
		
		Mockito.when(
					facadeRestController.post(Mockito.anyString(), Mockito.anyString(), Mockito.anyObject(), Mockito.any())
				).thenReturn("user");
				
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/login")
				.param("login", "user")
				.param("pwd", "azerty")
				.accept(MediaType.TEXT_PLAIN);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expected = "user";
		assertEquals(expected, result.getResponse().getContentAsString());
	}
	
	
}
