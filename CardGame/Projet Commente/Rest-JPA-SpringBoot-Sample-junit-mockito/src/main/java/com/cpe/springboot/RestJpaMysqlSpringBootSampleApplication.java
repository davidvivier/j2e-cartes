package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe principale de l'application, qui contient le main() de départ
 */
@SpringBootApplication
public class RestJpaMysqlSpringBootSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestJpaMysqlSpringBootSampleApplication.class, args);
	}
}
