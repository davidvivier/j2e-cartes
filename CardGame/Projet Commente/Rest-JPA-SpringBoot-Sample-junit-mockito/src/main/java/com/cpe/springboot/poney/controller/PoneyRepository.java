package com.cpe.springboot.poney.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.poney.model.Poney;

/**
 * Cette classe permet de faire l'interface avec la base de données pour facilement
 * 	effectuer les requêtes SELECT, UPDATE, DELETE en base de données
 *
 * On lui indique qu'elle travaille avec la classe Poney.
 *
 */
public interface PoneyRepository extends CrudRepository<Poney, Integer> {
	
	public List<Poney> findByColor(String color);

}
