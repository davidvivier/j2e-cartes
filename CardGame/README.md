# CardGame - Rendu atelier 3

## Auteurs :
- Yann Cayrac
- David Vivier
- Hamza Mourik
- Maxence Barjon
- Dylan Martins

Il est à noter qu'aucun de nous n'avait utilisé Springboot avant ce projet.

## Utilisation

Après avoir démarré les microservices (le microservice Room ne démarre pas), accéder à la page `127.0.0.1:8081/inscription.html` pour s'inscrire. Vous serez ensuite redirigé vers la page de login pour vous connecter.

Si vous souhaitez pouvoir vous connecter il vaut mieux utiliser la navigation privée (puis fermer la fenêtre pour être déconnecté) car nous utilisons un cookie pour retenir la connexion.

## Dépôt Git

Le projet est disponible à l'adresse https://gitlab.com/davidvivier/j2e-cartes

## Architecture de l'application et protocole de communication.

Nous avons développé cette application avec l'architecture décrite dans le document `JEE - Architecture.pdf`.
L'application est constituée de quatre micro-services dont un seul (Facade) est en interaction avec le client. Ces micro-services interagissent entre eux, selon un protocole que nous avons défini et décrit dans le document `JEE - Protocole.png`

# Base de données

D'après l'architecture que nous avons choisie, chaque microservice dispose de sa propre base de données. En effet la séparation des fonctionnalités implique de séparer également les données.
Dans un cas réel, on aurait donc trois bases de données séparées et associées chacune à son microservice.
Pour des raisons pratiques dans ce projet nous utiliserons une seule base de données contenant les tables des différents micro-services.

Le fichier de dump `Dump20180531.sql` permet de recréer la base de données et contient 1 utilisateur et 1 carte.


## Microservices 

Les quatre microservices de notre application sont chacun un projet maven à part entière, chacun situé dans les sous-dossiers correspondants :
- Facade
- User
- Card
- Room

Le projet commenté se trouve dans le dossier `Projet Commente`

### Facade 

`127.0.0.1:8081`

La Façade constitue l'interface entre le Client et le reste de l'application. Elle sert de routeur pour les requêtes et les réponses : en fonction des informations de la requête, elle choisit à quel microservice elle doit la relayer. Elle met également à disposition les pages html qui constituent la partie "front-end" de l'application, et qui utilisent des script js pour faire des requêtes vers la Façade.

Les fichiers de vue (html/css/js) sont situés dans le microservice Facade dans le dossier src/main/resources/static .

Toutes les communications décrites dans le protocole sont relayées et envoyées au bon endroit, et les réponses transférées au Client.


### User 

`127.0.0.1:8082`

Ce microservice gère tout ce qui à trait à la gestion des utilisateurs. 

### Card 

`127.0.0.1:8083`

Ce micro-service gère les cartes et le magasin.
Ses fonctionnalités sont développées et testables avec des requêtes GET dans le navigateur (cf. Protocole)
Les vues html/js ne sont pas encore au point pour utiliser ces fonctionnalités de manière graphique.

### Room 

`127.0.0.1:8084`

Ce micro-service est dédié à la gestion des combats du jeu. Il procède en deux étapes : le premier joueur envoie son choix de carte et crée la Room.
Le second joueur choisit sa carte et sa Room et envoie son choix. Le microservice Room effectue alors les différentes étapes de la mécanique de jeu définie pour un combat. Elle stocke le résultat en base de données.

Le micro-service Room est en partie développé, mais nous n'arrivons plus à le faire démarrer.


## Tests 

### Tests unitaires avec JUnit

Pour chacun des microservices nous avons écrit des tests unitaires pour certaines fonctions. 

### Jacoco

Nous avons utilisé le plugin Jacoco avec succès, cependant nous ne pouvons pas exploiter le résultat directement, il nous faut utiliser Sonar.

### Sonar 

Nous avons lancé le serveur Sonar mais il s'est avéré impossible (constaté en TP) d'exécuter la commande permettant de lancer l'analyse. 
Nous n'avons donc rien à fournir provenant de Sonar.



## Ce qui n'a pas été réalisé : 

- La vidéo de présentation
- Certaines méthodes n'ont pas de test unitaire
- Nous n'avons pas utilisé Postman
- Il n'y a pas de Plan de tests ni de représentation de la couverture de tests

## Contenu supplémentaire :

- Description détaillée et formalisée du protocole de communication


