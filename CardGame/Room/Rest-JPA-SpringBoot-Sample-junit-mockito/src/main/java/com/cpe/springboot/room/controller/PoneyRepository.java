package com.cpe.springboot.room.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.room.model.Poney;

public interface PoneyRepository extends CrudRepository<Poney, Integer> {
	
	public List<Poney> findByColor(String color);

}
