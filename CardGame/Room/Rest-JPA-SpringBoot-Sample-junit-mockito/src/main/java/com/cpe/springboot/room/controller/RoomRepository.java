package com.cpe.springboot.room.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.room.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {
	public List<Room> findAll();
}
