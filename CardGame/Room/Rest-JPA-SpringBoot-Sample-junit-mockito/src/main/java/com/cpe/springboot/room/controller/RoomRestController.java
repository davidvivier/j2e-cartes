package com.cpe.springboot.room.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.room.model.CardModel;
import com.cpe.springboot.room.model.Room;


@RestController
public class RoomRestController {
	@Autowired
	private RoomRepository roomRepository;
	
	private RestTemplate rest = new RestTemplate();
	
	private final String BASE_URL = "http://127.0.0.1:";
	private final String FACADE_PORT = "8081";
	private final String USER_PORT = "8082";
	private final String CARD_PORT = "8083";
	private final String ROOM_PORT = "8084";

	
	private <T> T post(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> requestBody = new HttpEntity<>("", headers);
		T res = rest.postForObject(BASE_URL + target + url, requestBody, responseType, params);
		return res;
	}
	
	private <T> T get(String url, String target, HashMap<String, String> params, Class<T> responseType) {
		T res = rest.getForObject(BASE_URL + target + url, responseType, params);
		return res;
	}
	
	@RequestMapping("/rooms")
	private List<Room> getAllRooms() {
		List<Room> rooms = new ArrayList<>();
		roomRepository.findAll().forEach(rooms::add);
		for(int i = 0; i < rooms.size(); i++){
			if(rooms.get(i).getUseurGagnant() != 0) {
				rooms.remove(i);
			}
		}
		return rooms;
	}
	
	@RequestMapping("/rooms/create/user/{userId}/carte/{cardId}")
	private int getNewRoom(@PathVariable int userId, @PathVariable int cardId) {
		HashMap<String, String> params = new HashMap<String, String>();
		
		// Asc card microServis for card id and if card is owned by the user of the id
		CardModel card = get("/cardofuser/"+cardId+"/"+userId, CARD_PORT, params, CardModel.class);
		
		// controle de la valididé de la carte 
		
		
		// return room id
		Room room = new Room(userId, cardId);
		return room.getId();
	}
	
	@RequestMapping("/rooms/join/{roomId}/user/{userId}/carte/{cardId}")
	private void joinRoom(@PathVariable int roomId, @PathVariable int userId, @PathVariable int cardId) {
		HashMap<String, String> params = new HashMap<String, String>();
		
		// Asc card microServis for card id and if card is owned by the user of the id
		CardModel card2 = get("/cardofuser/"+cardId+"/"+userId, CARD_PORT, params, CardModel.class);
		
		// controle de la valididé de la carte 
		
		
		// récupération de la room 
		Room room = roomRepository.findOne(Integer.valueOf(roomId));
		room.setUseur2(userId);
		room.setIdCard2(cardId);
		
		// récupération de la carte1
		CardModel card1 = get("/cardofuser/"+room.getIdCard1()+"/"+room.getUseur1(), CARD_PORT, params, CardModel.class);
		
		// bataye 
		while(card1.getHp() > 0 && card2.getHp() < 0) {
			card1.setHp(card1.getHp() - card2.getAttack());
			if(card1.getHp() > 0) {
				card2.setHp(card2.getHp() - card1.getAttack());
			}
		}
		if(card1.getHp() <= 0) {
			room.setCardGagnant(card2.getId());
			room.setUseurGagnant(room.getUseur2());
		}else {
			room.setCardGagnant(card1.getId());
			room.setUseurGagnant(room.getUseur1());
		}
		
	}
	
	@RequestMapping("/rooms/state/{roomId}")
	private int getNewRoom(@PathVariable int roomId) {
		
		Room room = roomRepository.findOne(Integer.valueOf(roomId));
		return room.getUseurGagnant(); 
		
	}
	
}
