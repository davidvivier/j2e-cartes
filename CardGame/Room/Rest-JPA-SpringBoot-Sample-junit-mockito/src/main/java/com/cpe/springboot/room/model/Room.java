package com.cpe.springboot.room.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Room {

	public Room(Integer useur1, Integer idCard1) {
		super();
		this.useur1 = useur1;
		this.idCard1 = idCard1;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
	private Integer useur1;
	private Integer useur2; 
	private Integer idCard1; 
	private Integer idCard2;
	private Integer useurGagnant; 
	private Integer CardGagnant; 
	
	public Integer getUseurGagnant() {
		return useurGagnant;
	}
	public void setUseurGagnant(Integer useurGagnant) {
		this.useurGagnant = useurGagnant;
	}
	public Integer getCardGagnant() {
		return CardGagnant;
	}
	public void setCardGagnant(Integer cardGagnant) {
		CardGagnant = cardGagnant;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUseur1() {
		return useur1;
	}
	public void setUseur1(Integer useur1) {
		this.useur1 = useur1;
	}
	public Integer getUseur2() {
		return useur2;
	}
	public void setUseur2(Integer useur2) {
		this.useur2 = useur2;
	}
	public Integer getIdCard1() {
		return idCard1;
	}
	public void setIdCard1(Integer idCard1) {
		this.idCard1 = idCard1;
	}
	public Integer getIdCard2() {
		return idCard2;
	}
	public void setIdCard2(Integer idCard2) {
		this.idCard2 = idCard2;
	}

}
