package com.cpe.springboot.room.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.room.controller.RoomRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomTest {
	
	@Autowired
	private RoomRepository roomRepository;
	
	@Test
    public void createRoom() {
		roomRepository.save(new Room(1,1));
		List<Room> roomList= roomRepository.findAll();
		assertTrue(roomList.size() ==1);
		assertTrue(roomList.get(0).getUseur1().equals(1));
    }
}


