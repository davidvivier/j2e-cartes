package com.cpe.springboot.controller;

//import static org.mockito.Matchers.isNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.model.UserModel;


@RestController
public class UserRestController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/users")
	private List<UserModel> getAllCourses() {
		return userService.getAllUsers();

	}	
	
	
	@RequestMapping("/signup/{login}/{pwd}/{name}")
	private String signup(@PathVariable("login") String login,
			@PathVariable("pwd") String pwd,
			@PathVariable("name") String name) {		
		return userService.setInfoSignUp(login, name, pwd);
	}
		
	
	@RequestMapping("/login/{login}/{pwd}")
	private String login(@PathVariable("login") String login,
			@PathVariable("pwd") String pwd
			) {
		UserModel user = userService.getUser(login);
		String reponse;
		if (user != null) {			
			if(user.getPwd().equals(pwd)) {
				reponse= login;
			} else {
				reponse = "badpassword";
			}			
		}else {
			reponse= "loginnotfound";
		}		
		return reponse;
	}
	
	@RequestMapping("/user/{login}/money")
	private int money(@PathVariable String login
			) {
		UserModel user = userService.getUser(login);
		return user.getMoney();	
	}
	
	
	@RequestMapping("/user/{login}/spend/{amount}")
	private boolean spend(@PathVariable String login,
			@PathVariable int amount
			) {		
		return userService.spendMoney(login, amount);		
	}
	
	@RequestMapping("/user/{login}/earn/{amount}")
	private boolean earn(@PathVariable String login,
			@PathVariable int amount
			) {	
		return userService.earnMoney(login, amount);			
	}
	
	@RequestMapping("/user/info/{login}")
	private UserModel info(@PathVariable String login
			) {			
		return userService.getUser(login);			
	}
	
	/*@RequestMapping("/users/{id}")
	private UserModel getUser(@PathVariable String id) {
		return userService.getUser(id);

	}*/
	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public void addUser(@RequestBody UserModel user) {
		userService.addUser(user);
	}	
	

}
