package com.cpe.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<UserModel> getAllUsers() {
		List<UserModel> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;
	}

	public UserModel getUser(String login) {
		List<UserModel> users = getAllUsers();
		for(UserModel u : users) {
			if (u.getLogin().equals(login)) {
				return u;
			}
		}
		return null;
	}
	
	public Integer getMoneyOfUser(String login) {
		UserModel user = this.getUser(login);
		return user.getMoney();
	}
	
	public String setInfoSignUp(String login,String name,String pwd) {
		UserModel user = this.getUser(login);
		if (null != user) {
			return "exists";
		}		
		user = new UserModel();
		user.setLogin(login);
		user.setPwd(pwd);
		user.setName(name);
		user.setMoney(0);
		this.addUser(user);
		return login;
	}
	
	public boolean spendMoney(String login,int amount) {
		UserModel user = this.getUser(login);
		if(user.getMoney()>=amount) {
			int newMoney= user.getMoney()-amount;
			user.setMoney(newMoney);
			this.updateUser(user);
			return true;
		}else {
			return false;
		}
	}
	
	public boolean earnMoney(String login,int amount) {
		UserModel user = this.getUser(login);
		int newMoney= user.getMoney()+amount;
		user.setMoney(newMoney);
		this.updateUser(user);
		return true;
	}
	
	

	public void addUser(UserModel user) {
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.delete(Integer.valueOf(id));
	}

}
