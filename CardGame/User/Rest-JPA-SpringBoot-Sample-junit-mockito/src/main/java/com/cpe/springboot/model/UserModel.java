package com.cpe.springboot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserModel {
	
	@Id
	private String login;
	private String pwd;
	private String name;
	private int money;
	
	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.name = "";
		this.money= 0;
	}
	
	public UserModel( String login,String name,String pwd, int money) {
		this.login = login;
		this.pwd = pwd;
		this.name = name;
		this.money = money;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

}
