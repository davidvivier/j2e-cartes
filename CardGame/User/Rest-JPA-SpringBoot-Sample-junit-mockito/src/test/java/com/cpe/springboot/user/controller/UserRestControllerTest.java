package com.cpe.springboot.user.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cpe.springboot.controller.UserRestController;
import com.cpe.springboot.model.UserModel;
import com.cpe.springboot.controller.UserRestController;
import com.cpe.springboot.controller.UserService;
import com.cpe.springboot.model.UserModel;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRestController.class, secure = false)
public class UserRestControllerTest {
	
	@Autowired
	private UserRestController rest;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService userService;

	UserModel mockUser= new UserModel("logi", "nam", "pwd1", 2);
	
	@Test
	public void testSpend() throws Exception {
		Mockito.when(
				userService.spendMoney(Mockito.anyString(),Mockito.anyInt())
				).thenReturn(true);
			
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/logi/spend/2")
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooo");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("true", result.getResponse().getContentAsString());
	}
	
	@Test
	public void testEarn() throws Exception {
		Mockito.when(
				userService.earnMoney(Mockito.anyString(), Mockito.anyInt())
				).thenReturn(true);
			
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/logi/earn/8")
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooo");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("true", result.getResponse().getContentAsString());
	}
	
	@Test
	public void testMoney() throws Exception {
		Mockito.when(
				userService.getUser(Mockito.anyString())
				).thenReturn(mockUser);
			
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/user/logi/money")
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("oooooooooooooooney");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("2", result.getResponse().getContentAsString());
	}
	
	@Test
	public void testLogin() throws Exception {
		
		// cas login inexistant
		
		Mockito.when(
				userService.getUser(Mockito.anyString())
				).thenReturn(null);
			
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/login")
				.param("login", "logia")
				.param("pwd", "pwd1")
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooongin");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("loginnotfound", result.getResponse().getContentAsString());
		
		
		// cas login existant, mauvais password

		
		Mockito.when(
				userService.getUser(Mockito.anyString())
				).thenReturn(mockUser);
			
		requestBuilder = MockMvcRequestBuilders
				.post("/login")
				.param("login", "logi")
				.param("pwd", "wrongpassword")
				.accept(MediaType.APPLICATION_JSON);
		
		result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooongin");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("badpassword", result.getResponse().getContentAsString());
		
		
		// cas login existant, bon password

		
		Mockito.when(
				userService.getUser(Mockito.anyString())
				).thenReturn(mockUser);
			
		requestBuilder = MockMvcRequestBuilders
				.post("/login")
				.param("login", "logi")
				.param("pwd", "pwd1")
				.accept(MediaType.APPLICATION_JSON);
		
		result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooongin");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("logi", result.getResponse().getContentAsString());

	
	}
	
	
	@Test
	public void testSignUp() throws Exception {
		
		// cas user existant
		
		Mockito.when(
				userService.setInfoSignUp(Mockito.anyString(), 
						Mockito.anyString(), Mockito.anyString())
				).thenReturn("exists");
		
			
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/signup")
				.param("login", "logi")
				.param("name", "nam")
				.param("pwd", "pwd1")				
				.accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooonupp");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("exists", result.getResponse().getContentAsString());
	
	
		// cas user inexistant		
		
		Mockito.when(
				userService.setInfoSignUp(Mockito.anyString(), 
						Mockito.anyString(), Mockito.anyString())
				).thenReturn("logi");
		
			
		requestBuilder = MockMvcRequestBuilders
				.post("/signup")
				.param("login", "logi")
				.param("name", "nam")
				.param("pwd", "pwd1")				
				.accept(MediaType.APPLICATION_JSON);
		
		result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println("ooooooooooooooonupp");
		System.out.println(result.getResponse().getContentAsString());
		System.out.println("ooooooooooooooo");	
		
		assertEquals("logi", result.getResponse().getContentAsString());
	
	}
	
}