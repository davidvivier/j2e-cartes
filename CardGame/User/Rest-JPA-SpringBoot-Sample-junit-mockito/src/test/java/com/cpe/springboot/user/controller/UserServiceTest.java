package com.cpe.springboot.user.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.controller.UserRepository;
import com.cpe.springboot.controller.UserService;
import com.cpe.springboot.model.UserModel;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {
	
	@Autowired
	private UserRepository userRepository;	
	

	private UserService userService;
	
	@Test
    public void saveUser() {
		userRepository.save(new UserModel("log", "nam", "pwd", 5));
		List<UserModel> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add); 
		assertTrue(users.size() ==1);
		assertTrue(users.get(0).getLogin().equals("log"));
		assertTrue(users.get(0).getName().equals("nam"));
		assertTrue(users.get(0).getPwd().equals("pwd"));
		assertTrue(users.get(0).getMoney()==5);
	}
	
//	@Test
//    public void spendMoney() {
//		UserModel mockedUser = new UserModel("log", "nam", "pwd", 5);
//		List<UserModel> users = new ArrayList<>();
//		users.add(mockedUser);
//		Mockito.when(
//				userRepository.findByLogin(Mockito.anyString())
//				).thenReturn(users);
//		
//		boolean res = userService.spendMoney("logi", 10);
//		
//		assertFalse(res);
//	}
	
	
}
