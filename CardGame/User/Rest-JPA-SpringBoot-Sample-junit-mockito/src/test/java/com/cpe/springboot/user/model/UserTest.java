package com.cpe.springboot.user.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cpe.springboot.controller.UserRepository;
import com.cpe.springboot.model.UserModel;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserTest {
	
	@Autowired
	private UserRepository userRepository;

	 @Test
	    public void creatUser() {
		 userRepository.save(new UserModel("log","nam","123mdp",12));
		 List<UserModel> userList= userRepository.findByLogin("log");
		 assertTrue(userList.size() ==1);
		 assertTrue(userList.get(0).getName().equals("nam"));
	    }
}
