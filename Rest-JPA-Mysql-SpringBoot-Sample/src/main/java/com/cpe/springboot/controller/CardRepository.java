package com.cpe.springboot.controller;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.model.CardModel;

public interface CardRepository extends CrudRepository<CardModel, Integer> {


}
