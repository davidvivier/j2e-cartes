package com.cpe.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.model.CardModel;

@RestController
public class CardRestController {
	
	@Autowired
	private CardService cardService;
	
	@RequestMapping("/cards")
	private List<CardModel> getAllCards() {
		return cardService.getAllCards();
	}
	
	@RequestMapping("/card/{id}")
	private CardModel getCard(@PathVariable String id) {
		return cardService.getCard(id);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public void addCard(@RequestBody CardModel card) {
		cardService.addCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
	public void updateCard(@RequestBody CardModel card,@PathVariable String id) {
		card.setId(Integer.valueOf(id));
		cardService.updateCard(card);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/card/{id}")
	public void deleteCard(@PathVariable String id) {
		cardService.deleteCard(id);
	}

}
