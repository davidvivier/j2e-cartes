package com.cpe.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.model.CardModel;

@Service
public class CardService {

	@Autowired
	private CardRepository cardRepository;

	public List<CardModel> getAllCards() {
		List<CardModel> cards = new ArrayList<>();
		cardRepository.findAll().forEach(cards::add);
		return cards;
	}

	public CardModel getCard(String id) {
		return cardRepository.findOne(Integer.valueOf(id));
	}

	public void addCard(CardModel card) {
		cardRepository.save(card);
	}

	public void updateCard(CardModel card) {
		cardRepository.save(card);
	}

	public void deleteCard(String id) {
		cardRepository.delete(Integer.valueOf(id));
	}

}
