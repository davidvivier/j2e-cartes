package com.cpe.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.model.UserModel;

@RestController
public class UserRestController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/users")
	private List<UserModel> getAllCourses() {
		return userService.getAllUsers();

	}
	
	@PostMapping("/signup")
	private String signup(@RequestParam("login") String login,
			@RequestParam("pwd") String pwd,
			@RequestParam("name") String name) {
		UserModel user = userService.getUser(login);
		if (null != user) {
			return "alreadyexists";
		}
		user = new UserModel();
		user.setLogin(login);
		user.setPwd(pwd);
		user.setName(name);
		user.setMoney(0);
		userService.addUser(user);
		return login;
	}
	
	/*@RequestMapping("/users/{id}")
	private UserModel getUser(@PathVariable String id) {
		return userService.getUser(id);

	}*/
	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public void addUser(@RequestBody UserModel user) {
		userService.addUser(user);
	}	

}
