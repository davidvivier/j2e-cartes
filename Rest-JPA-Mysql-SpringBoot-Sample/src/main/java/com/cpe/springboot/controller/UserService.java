package com.cpe.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public List<UserModel> getAllUsers() {
		List<UserModel> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		return users;
	}

	public UserModel getUser(String login) {
		List<UserModel> users = getAllUsers();
		for(UserModel u : users) {
			if (u.getLogin().equals(login)) {
				return u;
			}
		}
		return null;
	}

	public void addUser(UserModel user) {
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.delete(Integer.valueOf(id));
	}

}
