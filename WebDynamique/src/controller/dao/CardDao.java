package controller.dao;

import model.CardModel;

public class CardDao {
	
	private final static String DB_LOCATION = "db-tp.cpe.fr";
	private final static int DB_PORT = 3306;
	private final static String DB_NAME = "binome36";
	private final static String DB_USER = "binome36";
	private final static String DB_PWD = "binome36";

	private String dblocation;
	private int dbport;
	private String dbName;
	private String username;
	private String pwd;

	public CardDao(String dblocation, int dbport, String dbName, String username, String pwd) {
		this.dblocation = dblocation;
		this.dbport = dbport;
		this.dbName = dbName;
		this.username = username;
		this.pwd = pwd;
	}
	public CardDao() {
		this.dblocation = DB_LOCATION;
		this.dbport = DB_PORT;
		this.dbName = DB_NAME;
		this.username = DB_USER;
		this.pwd = DB_PWD;
	}
		
}