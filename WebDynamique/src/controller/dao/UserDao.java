package controller.dao;

import java.util.ArrayList;
import java.util.List;
import model.UserBean;

public class UserDao {
	private List<UserBean> userList;
	
	public UserDao() {
		userList = new ArrayList<>();
		UserBean u1 = new UserBean();
		u1.setLogin("u1");
		u1.setPwd("pwd1");
		u1.setName("Name1");
		userList.add(u1);
		
		UserBean u2 = new UserBean();
		u2.setLogin("u1");
		u2.setPwd("pwd1");
		u2.setName("Name");
		userList.add(u2);
	}
	
	public boolean checkUser(String login, String pwd) {
		for (UserBean u : userList) {
			if (u.getLogin().equals(login) && u.getPwd().equals(pwd)) {
				return true;
			}
		}
		return false;
	}
	
	public String getUname(String login) {
		for (UserBean u : userList) {
			if (u.getLogin().equals(login)) {
				return u.getName();
			}
		}
		return null;
	}
	
	public UserBean getUser(String login) {
		for (UserBean u : userList) {
			if (u.getLogin().equals(login)) {
				return u;
			}
		}
		return null;
	}
}
