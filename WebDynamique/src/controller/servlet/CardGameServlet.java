package controller.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.dao.UserDao;

/**
 * Servlet implementation class CardGameServlet
 */
@WebServlet("/CardGameServlet")
public class CardGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao uDao;
	private static final String LOGIN_ATT = "LOGIN";
	private static final String PWD_ATT = "PWD";

	
    public CardGameServlet() {
        super();
        this.uDao = new UserDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String resultStr = "";
		
		//response.setContentType("text/html");
		String login = (String) request.getParameter(LOGIN_ATT);
		String pwd = (String) request.getParameter(PWD_ATT);
		
		boolean result = this.uDao.checkUser(login, pwd);
		if (result) {
			resultStr = resultStr + "success";
			request.getSession().setAttribute("user_name", uDao.getUname(login));
			request.getSession().setAttribute("user", uDao.getUser(login));

			// redirect
			this.getServletContext().getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		} else {
			resultStr = resultStr + "fail";
		}
		
		response.getWriter().append(resultStr);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
