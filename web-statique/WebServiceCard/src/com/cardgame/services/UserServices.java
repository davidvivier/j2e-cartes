package com.cardgame.services;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.cardgame.controler.CardDao;
import com.cardgame.model.CardModel;
import com.cardgame.utils.Tools;

@Path("/servicescard")
public class UserServices {
	private CardDao dao;
	
	@Context
	ServletContext context;
	
	public UserServices() {
	}
	
	/*
	 * Renvoi la liste des cartes sous format JSON
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON) //La méthode doit renvoyer une réponse sous forme de réponse Json
	@Path("/cards")
	public String getCards() {
		getDao(); //Récupère ou créé un objet de type DAO
		String cardsJson=this.dao.getCardsJson(); //Appelle à la fonction de récupération des cartes du dao
		return cardsJson;
	}

	/*
	 * Renvoi une carte identifiée par son id sous format JSON
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/card/{cardId}")
	public String getCardById(
			@PathParam("cardId") String cardId) {
		getDao();
		return Tools.toJsonString(this.dao.getCard(cardId));
	}
	
	/*
	 * Renvoi une carte d'après son nom
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/find")
	public String findCard(@QueryParam("name") String cardName) {
		getDao();
		String jsonCard=Tools.toJsonString(this.dao.getCardByName(cardName));
		return jsonCard;
	}
	
	/*
	 * Créé une nouvelle carte
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/add")
	public String findCard(CardModel newCard) {
		getDao();
		this.dao.addCard(newCard);
		return "{'request:'success'}";
	}
	
	/*
	 * Crée l'objet DAO
	 */
	public CardDao getDao(){
		if(context.getAttribute("DAO")!=null){
			this.dao=(CardDao)context.getAttribute("DAO");
		}else{
			this.dao=new CardDao();
			context.setAttribute("DAO",this.dao);
		}
		return this.dao;
	}

}
